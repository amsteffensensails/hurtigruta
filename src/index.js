import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Icon from "./assets/magnifying-glass.svg";
import IconClose from "./assets/cross.svg";

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imgSrc: Icon
    };
    this.handleFocus = this.handleFocus.bind(this);
    this.handleFocusOut = this.handleFocusOut.bind(this);
  }

  handleFocus() {
    this.setState({
      imgSrc: IconClose
    });
  }

  handleFocusOut() {
    this.setState({
      imgSrc: Icon
    });
  }

  render() {
    return (
      <form className="search">
        <p className="search__error">
          The input is not valid. It must not be empty!
        </p>
        <div role="search" className="search__field">
          <label className="search__label" htmlFor="search">
            Search
          </label>
          <input
            placeholder="Search"
            type="text"
            className="search__input"
            id="search"
            onFocus={this.handleFocus}
            focusout={this.handleFocusOut}
            required
          />
          <img src={this.state.imgSrc} class="search__icon" alt="" />
        </div>
      </form>
    );
  }
}
ReactDOM.render(<Search />, document.getElementById("root"));
